from subprocess import call
import time
SLEEP_TIME = 5
while(True):
    print('Starting program')
    call(['./bgp_stream_parser.py'])
    print('Stopping program waiting %s seconds' % SLEEP_TIME)
    time.sleep(SLEEP_TIME)

