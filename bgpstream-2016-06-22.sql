-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2016 at 10:01 AM
-- Server version: 5.7.12-0ubuntu1
-- PHP Version: 7.0.4-7ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bgpstream`
--

-- --------------------------------------------------------

--
-- Table structure for table `bgp_elements`
--

CREATE TABLE `bgp_elements` (
  `bgp_element_id` bigint(20) UNSIGNED NOT NULL,
  `record_id_owner` bigint(20) UNSIGNED NOT NULL,
  `element_time` bigint(20) NOT NULL,
  `peer_address` tinytext COLLATE utf8_swedish_ci NOT NULL,
  `peer_asn` int(11) NOT NULL,
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bgp_records`
--

CREATE TABLE `bgp_records` (
  `bgp_record_id` bigint(20) UNSIGNED NOT NULL,
  `project` tinytext COLLATE utf8_swedish_ci NOT NULL,
  `collector` tinytext COLLATE utf8_swedish_ci NOT NULL,
  `record_time` bigint(20) NOT NULL,
  `dump_time` bigint(20) NOT NULL,
  `type` enum('update','rib','unknown') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'update',
  `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `record_count`
--
CREATE TABLE `record_count` (
);

-- --------------------------------------------------------

--
-- Structure for view `record_count`
--
DROP TABLE IF EXISTS `record_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `record_count`  AS  select `bgp_records`.`peer_ip` AS `peer_ip`,`bgp_records`.`prefix` AS `prefix`,`bgp_records`.`record_timestamp` AS `record_timestamp`,count(0) AS `record_count` from `bgp_records` group by `bgp_records`.`peer_ip`,`bgp_records`.`prefix`,`bgp_records`.`record_timestamp` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bgp_elements`
--
ALTER TABLE `bgp_elements`
  ADD PRIMARY KEY (`bgp_element_id`);

--
-- Indexes for table `bgp_records`
--
ALTER TABLE `bgp_records`
  ADD PRIMARY KEY (`bgp_record_id`),
  ADD UNIQUE KEY `compound_key` (`project`(100),`collector`(100),`record_time`,`dump_time`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bgp_elements`
--
ALTER TABLE `bgp_elements`
  MODIFY `bgp_element_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7349;
--
-- AUTO_INCREMENT for table `bgp_records`
--
ALTER TABLE `bgp_records`
  MODIFY `bgp_record_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144095;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
