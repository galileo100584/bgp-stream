#!/usr/bin/env python

from _pybgpstream import BGPStream, BGPRecord, BGPElem
from collections import defaultdict
import time
import datetime
import os
import utils

# Create a new bgpstream instance and a reuseable bgprecord instance
stream = BGPStream()	
rec = BGPRecord()

collector_name = 'rrc11'
stream.add_filter('collector', collector_name)

t_end = int(time.time())	
t_start = t_end - 60 * 10  # the time interval (duration) we are getting from collector
stream.add_interval_filter(t_start, t_end)
print "Interval span {} secs".format(t_end - t_start)
 
# Start the stream
stream.start()

dump_list= set()

# records = {elem.peer_address: {'min': {min_truntacted: [elem.fields.prefix, ....], 'hour': ..., 'day': ... } } }
records = {}

# Get next record
while(stream.get_next_record(rec)):
    if rec.status == "valid" and rec.type != "rib":
        elem = rec.get_next_elem()
        entry = None
        # Try and get a entry from the record
        try:
            entry = records.get(elem.peer_address)
            if not entry:
                # Entry doesnt exists, create a new one
                entry = records[elem.peer_address] = {'min': {}, 'hour': {}, 'day': {}}
        except:
            pass

        while(elem):

            # Create timestamps by trunctaion
            t_min = int(elem.time / 60.0) * 60	
            t_hour = int(elem.time / (60.0 * 60.0)) * 60 * 60	
            t_day = int(elem.time / (60.0 * 60.0 * 24.0)) * 60 * 60 * 24


            # Using the timestamps as keys, add the prefixes to a list

            if not entry['min'].get(t_min):
                entry['min'][t_min] = []
            entry['min'][t_min].append(elem.fields.get('prefix')) 

            if not entry['hour'].get(t_hour):
                entry['hour'][t_hour] = []
            entry['hour'][t_hour].append(elem.fields.get('prefix')) 

            if not entry['day'].get(t_day):
                entry['day'][t_day] = []
            entry['day'][t_day].append(elem.fields.get('prefix')) 

            elem = rec.get_next_elem()

    dump_list.add(rec.dump_time)



# first = list(records.keys())[0]
# utils.pprint_color(records[first])
for ip, timestamps in records.iteritems():
    print(ip)
    for timestamp, prefixes in timestamps.items():
        print(timestamp, len(prefixes))


print "Dump times available from {} the last {} seconds: ".format(collector_name, t_end - t_start)
for i in dump_list:	 
	print str(i) + " =", 
	print (datetime.datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S\n'))

