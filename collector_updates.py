#!/usr/bin/env python
# -*- coding: utf-8 -*-
from _pybgpstream import BGPStream, BGPRecord, BGPElem
from collections import defaultdict
import time
import datetime
import os
import MySQLdb


db = MySQLdb.connect(user="bgpstream", host="localhost", passwd="Bgpstream9", db="bgpstream_copy")
db_cursor = db.cursor()


# Create a new bgpstream instance and a reusable bgprecord instance
stream = BGPStream()	
rec = BGPRecord()

# Consider Route Views origon only
collector_name = 'rrc11'
stream.add_filter('collector',collector_name)	#maybe we want route-views4?

t_end = int(time.time())	#current time now	
t_start = t_end-3600 #the time interval (duration) we are getting from collecor, e.i 60*60 = 3600s = 1 hour
stream.add_interval_filter(t_start,t_end)
print "Total duration " + str(t_end-t_start) + " sec"
 
# Start the stream
stream.start()

total_count = 0
local_count = 0
main_entry = []	#the hole list of peer_ip
time_stamps_record = []
time_stamps_dump = []
minute_list=[]
hour_list=[]
day_list=[]

dump_list=[]
constant_time = 1466345700	#random time_stamp, will be changed later
# Get next record



### Insert loop ###
# This loop insert new records and tries not to over count the records

while(stream.get_next_record(rec)):
    # Print the record information only if it is not a valid record
    if rec.status != "valid":
        print rec.project, rec.collector, rec.type, rec.time, rec.status
    else:
        
        # Skip if rib
        if rec.type == "rib":
            continue
        
        print """INSERT INTO bgp_records 
          (project, collector, record_time, dump_time, hit_count) 
          VALUES 
          (
          '"""+str(rec.project)+"""',
          '"""+str(rec.collector)+"""',
          '"""+str(rec.time)+"""',
          '"""+str(rec.dump_time)+"""',
          '1'
          ) 
          ON DUPLICATE KEY UPDATE
          hit_count=hit_count+1
        """
        
        #Insert records
        db_cursor.execute("""INSERT INTO bgp_records 
          (project, collector, record_time, dump_time, hit_count) 
          VALUES 
          (
          '"""+str(rec.project)+"""',
          '"""+str(rec.collector)+"""',
          '"""+str(rec.time)+"""',
          '"""+str(rec.dump_time)+"""'
          ) 
          ON DUPLICATE KEY UPDATE
          hit_count=hit_count+1
        """)
        
        system.os()
        
        #get affected rows from insert
        affected_rows = db.affected_rows()
        
        # Skip if dulpicate record 
        if affected_rows <= 0:
            continue
        
        # Extract insert id of last inserted bgp record
        last_record_id = db.insert_id()
        
        print last_record_id
        
        # Traverse elements
        elem = rec.get_next_elem()
        while(elem):
        
            
            print last_record_id
        
            ## Dette bør kaste en exeption
            if elem == None:
                continue
            
                     
            # Insert element
            db_cursor.execute(
            """INSERT INTO bgp_elements
            (record_id_owner, element_time, peer_address, peer_asn) 
            VALUES 
            (
            '"""+str(last_record_id)+"""',
            '"""+str(elem.time)+"""',
            '"""+str(elem.peer_address)+"""',
            '"""+str(elem.peer_asn)+"""'
            ) 
            """)
            
            
            
            
            """         
           # Print record and elem information

            #print rec.type, rec.time,
            #print rec.dump_time
            #if rec.dump_time not in dump_list:
            dump_list.append(rec.dump_time)
            constant_time = dump_list[0]
                #print elem.type, elem.peer_address, elem.peer_asn, elem.fields['prefix']
                #if rec.dump_time == constant_time:
            main_entry.append(elem.peer_address)
            local_count += 1
            #save time stampes; first from record then collector:
            time_stamps_record.append(rec.time)
            time_stamps_dump.append(rec.dump_time)
            
            total_count += 1
"""

            
#            print str(elem.peer_address) + "|" + str(elem.time) + "|" + str(elem.type) + "|" + str(elem.peer_asn) 

            elem = rec.get_next_elem()




print "Dump times avilible from " + collector_name + " the last " + str(t_end-t_start) + " secounds :"
for i in dump_list:	 
	print str(i) + " =", 
	print (datetime.datetime.fromtimestamp(int(i)).strftime('%Y-%m-%d %H:%M:%S\n'))
	break


new_list = []
count = 0

for current in main_entry:
	if current not in new_list:
		new_list.append(current)	#putting in peer_IPs tha are not in list 
		count+=1
			

#using class objects to save data:
class Record(object):
	def __init__(self, peer_ip):
		self.peer_ip = peer_ip
	count = 0
	dump_time = 0
	Ts = []	#This must be a list
	
	Ts_trunc = 0
#    
#	def __repr__(self):
#        return str(self.peer_ip) + " | " + str(self.dump_time)+" | " + str(self.Ts[0]) +" | " + str(self.Ts_trunc) + " | " + str(self.count)


list_entrys = [ Record(new_list[i]) for i in range(count)]

for i in main_entry:
	for j in list_entrys:
		if i == j.peer_ip:
			j.Ts.append(time_stamps_record[j.count])	#uniq Ts most be set her, one IP has many record_times
			j.count+=1
			

for i in range(count):
	list_entrys[i].dump_time = time_stamps_dump[i]
	

#using dictonary insted of class objects:
updates = {}
k=0
#for i in new_list:
#print main_entry	
for i in main_entry:	
	#updates = {}
	#updates[i] = 0 	#filling updates with the peer_IP's and seting value to '0' i.e (the update count)
	trunc = int( time_stamps_record[k] / 60.0 )
	trunc = trunc * 60
	#if i not in updates:
	updates[i] = {}
	updates[i] = {time_stamps_record[k]:trunc}
	updates[i]
	k += 1
	#print i
	#for j in time_stamps_record:
	#	updates[i][str(j)] = trunc



print "Data structure of dict = peer_IP:{record_time:bin_placment}"
print updates
